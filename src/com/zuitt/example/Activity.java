package com.zuitt.example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

public class Activity {
    public static void main(String[] args) {

        String[] fruitsArr = new String[5];
        fruitsArr[0] = "apple";
        fruitsArr[1] = "avocado";
        fruitsArr[2] = "banana";
        fruitsArr[3] = "kiwi";
        fruitsArr[4] = "orange";

        Scanner fruitsScanner = new Scanner(System.in);

        System.out.println(Arrays.toString(fruitsArr));
        System.out.println("Which fruit would you like to get the index of?");

        String fruit = fruitsScanner.nextLine();
        int index = Arrays.binarySearch(fruitsArr,fruit);
        System.out.println("The index of " + fruit + " is: "+ index);




        ArrayList<String> list = new ArrayList<>();
        list.add("John");
        list.add("Jane");
        list.add("Chloe");
        list.add("Zoey");

        System.out.println("My friends are: " + list);


        HashMap<String, Integer> inventory = new HashMap<>();
        inventory.put("toothpaste", 15);
        inventory.put("toothbrush", 20);
        inventory.put("soap", 12);

        System.out.println("Our current inventory consists of: " + inventory);
    }
}

