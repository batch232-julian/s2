package com.zuitt.example;

import java.util.Scanner;

public class ControlStructures {
    public static void main(String[] args) {
//        Operators in Java
//        Arithmetic- +, -, *, /, %
//        Comparison- >, <, <=, <=, ==, !=
//        Logical- &&, ||, !
//        Assignment- =

//        Conditional Structures in Java
//        Syntax: if(condition){
//    }

     int num1 = 15;
     if(num1 % 5 == 0){
         System.out.println(num1 + " is divisble by 5.");
     }
     num1 = 36;
     if (num1 % 5 == 0){
         System.out.println(num1 + " is divisble by 5.");
     }else {
         System.out.println(num1 + " is not divisble by 5.");
     }

     Scanner numberScanner = new Scanner(System.in);
//     System.out.println("Enter a number: ");
//     int numEvenOdd = numberScanner.nextInt();
//     if(numEvenOdd % 2 == 0){
//         System.out.println(numEvenOdd + " is even!");
//     } else {
//         System.out.println(numEvenOdd + " is odd!");
//        }

     int x = 15;
     int y = 0;
     if (y == 0 || x == 15 || y != 0 ) System.out.println(true);

//     Ternary operaor
        int num2 = 24;
        Boolean result = (num2 > 0) ? true : false;
//        String result = (num2 > 0) ? Boolean.toString(true) : Boolean.toString(false);
        System.out.println(result);

//        Switch cases
//        contro floow structure that allow one code block to be run out of many other code block
//        this is often when the input is predictable

        System.out.println("Enter a number 1-4 to see a SM malls in one of the four directions:");
        int directionValue = numberScanner.nextInt();

        switch (directionValue){
            case 1:
                System.out.println("SM North Edsa");
                break;
            case 2:
                System.out.println("SM South Mall");
                break;
            case 3:
                System.out.println("SM City Taytay");
                break;
            case 4:
                System.out.println("Sm Manila");
                break;
            default:
                System.out.println("Out of Range");
        }

    }
}
